from flask import Flask, render_template, session, request

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True

from . import data


@app.context_processor
def inject_global_values():
    menu = getattr(data, 'menu')
    history = [data.all_tags[t] for t in session['history'][:-1]]
    return dict(menu=menu, nav_history=history, box=data.box)

@app.route('/')
@app.route('/view/<tag>')
def view(tag='home'):
    history = session.get('history', [])
    if tag == 'home':
        history = ['home']
    else:
        nav = request.args.get('nav', '')
        if not nav:
            history = ['home'] if tag == 'home' else ['home', tag]
        elif nav == 'add':
            if history[-1] != tag:
                history.append(tag)
        elif nav == 'rem':
            if history[-2] == tag:
                del history[-1]
    session['history'] = history
    elem = data.all_tags[tag]
    return render_template(elem.template, elem=elem)


app.secret_key = 'Foo_random_key (or not ...)'
