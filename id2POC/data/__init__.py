
from .classDef import Tag, Media, Video, Image, Audio
from faker import Faker
import random

fake = Faker('fr_FR')

def get_unique_name(name):
    if name in all_tags:
        extension = 1
        while '{}{}'.format(name, extension) in all_tags:
            extension += 1
        name = '{}{}'.format(name, extension)
    return name

def fake_tag():
    title = fake.sentence(3).split()
    name = get_unique_name(title[0])
    title = ' '.join([name] + title[1:])
    return Tag(name=name,
               title=title,
               description=fake.paragraph())


media_types = [Video, Image, Audio]
def fake_media():
    title = fake.sentence(3).split()
    name = get_unique_name(title[0])
    title = ' '.join([name] + title[1:])
    media_type = random.choice(media_types)
    arguments = {
       'name': name,
       'title': title,
       'description': fake.paragraph(),
       'text': "".join('<p>{}</p>'.format(p) for p in fake.paragraphs()),
       'language': random.choice(media_type.LANGUAGES),
       'age_category': random.choice(media_type.AGE_CATEGORIES),
       'original': ''
    }
    if media_type == Video:
        url_template = "http://public.kymeria.fr/id2POC/videos/video{}.mp4"
        arguments['original'] = url_template.format(random.randint(1, 5))
    return media_type(**arguments)

home = Tag('home',
           title="Bienvenu sur notre IdeasCube documentaire",
           description=fake.paragraph())

menu = Tag('menu', title='menu', description='A tag to set what to display in the menu')

all_tags = {
    'home': home
}

first_level_tags = [fake_tag() for i in range(5)]
for tag in first_level_tags:
    all_tags[tag.name] = tag
    tag.tag(home)

for tag in random.sample(first_level_tags, 5):
    tag.tag(menu)


second_level_tags = [fake_tag() for i in range(10)]
for second_tag in second_level_tags:
    all_tags[second_tag.name] = second_tag
    for first_tag in random.sample(first_level_tags, 3):
        second_tag.tag(first_tag)


medias = [fake_media() for i in range(40)]
for media in medias:
    all_tags[media.name] = media
    for tag in random.sample(second_level_tags, 2):
        media.tag(tag)

box = {
  'name': 'ACFPE BANGUI'
}

