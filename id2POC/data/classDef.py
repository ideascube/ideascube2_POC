
class Taggable:
    def __init__(self, name, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = name

    def tag(self, parentTag):
        parentTag.add_tagged(self)


class Displayable:
    TAG = 'tag'
    MEDIA = 'media'
    IMAGE = 'image'
    VIDEO = 'video'
    AUDIO = 'audio'

    def __init__(self, title, description, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title = title
        self.description = description


class Tag(Displayable, Taggable):
    kind = Displayable.TAG
    template = 'index.html'

    def __init__(self, name, title, description):
        super().__init__(name=name, title=title, description=description)
        self.taggeds = set()


    def add_tagged(self, tagged):
        self.taggeds.add(tagged)

    def all_medias(self):
        ret = set()
        for t in self.taggeds:
            ret = ret.union(t.all_medias())
        return ret

    def nb_videos(self):
        return sum(1 for media in self.all_medias() if media.kind==media.VIDEO)

    def nb_images(self):
        return sum(1 for media in self.all_medias() if media.kind==media.IMAGE)

    def nb_audios(self):
        return sum(1 for media in self.all_medias() if media.kind==media.AUDIO)


class Media(Displayable, Taggable):
    kind = Displayable.MEDIA
    template = 'media.html'

    LANGUAGES = ('Français (fr)', 'English (en)', 'Italiano (it)')
    AGE_CATEGORIES = ('all', 'children', '12+', '18+')

    def __init__(self, name, title, description, text, original, language, age_category):
        super().__init__(name=name, title=title, description=description)
        self.text = text
        self.language = language
        self.age_category = age_category
        self.original = original

    def all_medias(self):
        return set([self])

class Video(Media):
    kind = Displayable.VIDEO


class Audio(Media):
    kind = Displayable.AUDIO


class Image(Media):
    kind = Displayable.IMAGE
