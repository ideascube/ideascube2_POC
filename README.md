What ?
======

This is a POC (Proof Of Concept) of the potential next ideascube.


How ?
=====

Clone this repository and in a virtual environment, run:

```
pip install -r requirements.txt
./run_app.sh
```

Visit `http://localhost:5000` with your browser.



Problems ?
========== 

This is a POC, this is not finished :)

Data (even their identifier) are randomly generated at each restart of the application.

If you restart the application, the current url on your browser may be not valid anymore
because the id changed.
Go to `http://localhost:5000` to start from the home page.
